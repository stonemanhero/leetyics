<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tracked_sessions', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->foreignId('website_id')->constrained('websites')->cascadeOnDelete();
            $table->foreignId('campaign_id')->nullable()->constrained('campaigns')->cascadeOnDelete();
            $table->string('event');
            $table->string('tag')->nullable();
            $table->string('ip');
            $table->string('user_agent');
            $table->text('path');
            $table->text('href')->nullable();
            $table->dateTime('recorded_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tracked_sessions');
    }
};
