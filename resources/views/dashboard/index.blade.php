@extends('layouts.master')

@section('content')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $sectionTitle }}</h1>
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-12 mt-sm-2">
            <div class="card border-primary text-center">
                <div class="card-header">Websites</div>
                <div class="card-body text-primary">
                    <h1>{{ $totalWebsites }}</h1>
                    <div class="d-grid gap-2">
                        <a href="{{ route('websites.index') }}" class="btn btn-sm btn-primary"><i class="bi bi-eye"></i> View</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12 mt-sm-2">
            <div class="card border-success text-center">
                <div class="card-header">Campaigns</div>
                <div class="card-body text-success">
                    <h1>{{ $totalCampaigns }}</h1>
                    <div class="d-grid gap-2">
                        <a href="{{ route('campaigns.index') }}" class="btn btn-sm btn-success"><i class="bi bi-eye"></i> View</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12 mt-sm-2">
            <div class="card border-warning text-center">
                <div class="card-header">Sessions</div>
                <div class="card-body text-warning">
                    <h1>{{ $totalSessions }}</h1>
                    <div class="d-grid gap-2">
                        <a href="{{ route('websites.index') }}" class="btn btn-sm btn-warning"><i class="bi bi-eye"></i> View</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 mt-sm-2">

        </div>
    </div>

    @include('fragments.logs', ['title' => "Latest Sessions Today", 'pagination' => false])
@endsection

