@extends('layouts.master')

@section('content')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $sectionTitle }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('campaigns.create') }}" class="btn btn-sm btn-secondary"><i class="bi bi-folder-plus"></i> Create</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        @if(count($campaigns) > 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Created At</th>
                    <th scope="col">Status</th>
                    <th scope="col">Name</th>
                    <th scope="col">Website</th>
                    <th scope="col">UUID</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($campaigns as $campaign)
                    <tr>
                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $campaign->created_at)->format('d/m/Y H:i') }}</td>
                        <td>
                            @if($campaign->is_active)
                                <a href="/campaigns/{{ $campaign->id }}/status/stop" class="btn btn-sm btn-danger"><i class="bi bi-stop-circle"></i> Stop</a>
                            @else
                                <a href="/campaigns/{{ $campaign->id }}/status/start" class="btn btn-sm btn-primary"><i class="bi bi-play-fill"></i> Start</a>
                            @endif
                        </td>
                        <td>{{ $campaign->name }}</td>
                        <td><a target="_blank" href="{{ $campaign->website->url }}">{{ $campaign->website->url }}</a></td>
                        <td>{{ $campaign->uuid }}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Controls">
                                <button data-bs-toggle="modal" data-bs-target="#modal-{{ $campaign->id }}" class="btn btn-sm btn-success"><i class="bi bi-code-square"></i> Get Code</button>
                                <a href="/analytics?website_id={{ $campaign->website_id }}&campaign_id={{ $campaign->id }}" class="btn btn-sm btn-warning"><i class="bi bi-clipboard-data"></i> Analytics</a>
                            </div>
                        </td>
                    </tr>

                    <div id="modal-{{ $campaign->id }}" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tracking Code</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <textarea rows="3" class="form-control" readonly>{{ $campaign->website->url . '?campaign=' . $campaign->uuid }}</textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="bi bi-x-circle"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </tbody>
            </table>

            {{ $campaigns->links() }}
        @else
            @include('fragments.no-data')
        @endif
    </div>
@endsection

