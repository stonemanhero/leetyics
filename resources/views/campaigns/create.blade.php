@extends('layouts.master')

@section('content')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $sectionTitle }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('campaigns.index') }}" class="btn btn-sm btn-secondary"><i class="bi bi-arrow-left-short"></i> Go Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            @include('fragments.alerts')

            <form name="createCampaign" method="POST" action="{{ route('campaigns.store') }}">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Campaign Name" value="{{ old('name') }}">
                </div>

                <div class="mb-3">
                    <label for="website_id" class="form-label">Website</label>
                    <select name="website_id" id="website_id" class="form-select" aria-label="Default select example">
                        <option selected>Select...</option>
                        @foreach($websites as $website)
                            <option value="{{ $website->id }}"
                                @if(old('website_id') == $website->id)
                                    selected
                                @endif
                            >{{ $website->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea class="form-control" name="description" id="description" rows="3">{{ old('description') }}</textarea>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="bi bi-floppy"></i> Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection

