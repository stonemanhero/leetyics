<div class="row mt-4">
    <div class="col">
        <h4><i class="bi bi-activity"></i> {{ $title }}</h4>
        <div class="table-responsive">
            @if(count($logs) > 0)
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Last Record</th>
                        <th scope="col">Website</th>
                        <th scope="col">Campaign</th>
                        <th scope="col">Session</th>
                        <th scope="col">IP</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $log->recorded_at)->format('d/m/Y H:i') }}</td>
                            <td>{{ $log->website_name }} - {{ $log->website_url }}</td>
                            <td>{{ !empty($log->campaign_name) ? $log->campaign_name : 'n/a' }}</td>
                            <td>{{ $log->uuid }}</td>
                            <td>{{ $log->ip }}</td>
                            <td>
                                <a href="{{ route('analytics.show' , ['uuid' => $log->uuid]) }}" class="btn btn-sm btn-success"><i class="bi bi-eye"></i> View</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($pagination)
                    {{ $logs->links() }}
                @endif
            @else
                @include('fragments.no-data')
            @endif
        </div>
    </div>
</div>
