@extends('layouts.master')

@section('content')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $sectionTitle }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('websites.create') }}" class="btn btn-sm btn-secondary"><i class="bi bi-folder-plus"></i> Create</a>
            </div>
        </div>
    </div>

    @include('fragments.alerts')
    <div class="table-responsive">
        @if(count($websites) > 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Created At</th>
                    <th scope="col">Status</th>
                    <th scope="col">Name</th>
                    <th scope="col">URL</th>
                    <th scope="col">UUID</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($websites as $website)
                    <tr>
                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $website->created_at)->format('d/m/Y H:i') }}</td>
                        <td>
                            @if($website->is_active)
                                <a href="/websites/{{ $website->id }}/status/stop" class="btn btn-sm btn-danger"><i class="bi bi-stop-circle"></i> Stop</a>
                            @else
                                <a href="/websites/{{ $website->id }}/status/start" class="btn btn-sm btn-primary"><i class="bi bi-play-fill"></i> Start</a>
                            @endif
                        </td>
                        <td>{{ $website->name }}</td>
                        <td><a target="_blank" href="{{ $website->url }}">{{ $website->url }}</a></td>
                        <td>{{ $website->uuid }}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Controls">
                                <button data-bs-toggle="modal" data-bs-target="#modal-{{ $website->id }}" class="btn btn-sm btn-success"><i class="bi bi-code-square"></i> Get Code</button>
                                <a href="/analytics?website={{ $website->id }}" class="btn btn-sm btn-warning"><i class="bi bi-clipboard-data"></i> Analytics</a>
                            </div>
                        </td>
                    </tr>

                    <div id="modal-{{ $website->id }}" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tracking Code</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <textarea rows="3" class="form-control" readonly><script src="{{ URL::to('/tracker.js?uuid=' . $website->uuid ) }}"></script></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="bi bi-x-circle"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </tbody>
            </table>

            {{ $websites->links() }}
        @else
            @include('fragments.no-data')
        @endif
    </div>
@endsection

