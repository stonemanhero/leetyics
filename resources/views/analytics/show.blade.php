@extends('layouts.master')

@section('content')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $sectionTitle }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('analytics.index') }}" class="btn btn-sm btn-secondary"><i
                        class="bi bi-arrow-left-short"></i> Go Back</a>
            </div>
        </div>
    </div>

    @include('fragments.alerts')

    <form name="filter" action="{{ route('analytics.show', ['uuid' => $uuid]) }}" method="GET" class="p-3 bg-light">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <label for="event" class="form-label">Timeframe</label>
                <select name="event" id="event" class="form-select" aria-label="Default select example">
                    <option value="all">All</option>
                    @foreach(\App\Enums\Event::cases() as $case)
                        <option value="{{ $case->name }}"
                                @if($selectedEvent == $case->name)
                                    selected
                            @endif
                        >{{ $case->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 col-sm-12">
                <label for="timeframe" class="form-label">Timeframe</label>
                <select name="timeframe" id="timeframe" class="form-select" aria-label="Default select example">
                    @foreach(\App\Enums\TimeFrame::cases() as $case)
                        <option value="{{ $case->name }}"
                                @if($selectedTimeframe == $case->name)
                                    selected
                            @endif
                        >{{ $case->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 col-sm-12">
                <label for="order_by" class="form-label">Order By</label>
                <select name="order_by" id="order_by" class="form-select" aria-label="Default select example">
                    <option value="desc"
                            @if($selectedOrderBy == 'desc')
                                selected
                        @endif
                    >Newest
                    </option>
                    <option value="asc"
                            @if($selectedOrderBy == 'asc')
                                selected
                        @endif
                    >Oldest</option>
                </select>
            </div>
            <div class="col-md-3 col-sm-12">
                <label for="asd" class="form-label">&nbsp;</label>
                <br>
                <div class="btn-group" role="group" aria-label="Controls">
                    <button class="btn btn-primary"><i class="bi bi-code-square"></i> Filter</button>
                    <a href="{{ route('analytics.show', ['uuid' => $uuid]) }}" class="btn btn-secondary"><i
                            class="bi bi-clipboard-data"></i> Refresh</a>
                </div>
            </div>
        </div>
    </form>

    <div class="row mt-4">
        <div class="col">
            <h4><i class="bi bi-activity"></i> Tracked Data</h4>
            <div class="table-responsive">
                @if(count($logs) > 0)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Recorded At</th>
                            <th scope="col">Event</th>
                            <th scope="col">Tag</th>
                            <th scope="col">Current Path</th>
                            <th scope="col">Href</th>
                            <th scope="col">IP</th>
                            <th scope="col">User Agent</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($logs as $log)
                            <tr
                                @if($log->event == 'click')
                                    class="table-info"
                                @elseif($log->event == 'beforeunload')
                                    class="table-danger"
                                @endif
                            >
                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $log->recorded_at)->format('d/m/Y H:i') }}</td>
                                <td>{{ $log->eventName() }}</td>
                                <td>{{ $log->tag }}</td>
                                <td>{{ $log->path }}</td>
                                <td>{{ $log->href }}</td>
                                <td>{{ $log->ip }}</td>
                                <td>{{ $log->user_agent }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                    {{ $logs->links() }}

                @else
                    @include('fragments.no-data')
                @endif
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        function getCampaigns(object) {
            window.location.href = '/analytics?website_id=' + object.value;
        }
    </script>
@endsection
