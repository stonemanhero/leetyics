@extends('layouts.master')

@section('content')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $sectionTitle }}</h1>
    </div>

    @include('fragments.alerts')

    <form name="filter" action="{{ route('analytics.index') }}" method="GET" class="p-3 bg-light">
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <label for="website_id" class="form-label">Website</label>
            <select name="website_id" id="website_id" class="form-select" aria-label="Default select example" onchange="getCampaigns(this)">
                <option value="all">All</option>

                @foreach($websites as $website)
                    <option value="{{ $website->id }}"
                            @if($selectedWebsite == $website->id)
                                selected
                        @endif
                    >{{ $website->name }} - {{ $website->url }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <label for="campaign_id" class="form-label">Campaigns</label>
            <select name="campaign_id" id="campaign_id" class="form-select" aria-label="Default select example" {{ $selectedWebsite == 'all' ? 'disabled' : '' }}>
                <option value="all">All</option>

                @foreach($campaigns as $campaign)
                    <option value="{{ $campaign->id }}"
                            @if($selectedCampaign == $campaign->id)
                                selected
                        @endif
                    >{{ $campaign->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <label for="timeframe" class="form-label">Timeframe</label>
            <select name="timeframe" id="timeframe" class="form-select" aria-label="Default select example">
                @foreach(\App\Enums\TimeFrame::cases() as $case)
                    <option value="{{ $case->name }}"
                        @if($selectedTimeframe == $case->name)
                            selected
                        @endif
                    >{{ $case->value }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <label for="asd" class="form-label">&nbsp;</label>
            <br>
            <div class="btn-group" role="group" aria-label="Controls">
                <button class="btn btn-primary"><i class="bi bi-code-square"></i> Filter</button>
                <a href="{{ route('analytics.index') }}" class="btn btn-secondary"><i class="bi bi-clipboard-data"></i> Refresh</a>
            </div>
        </div>
    </div>
    </form>

    @include('fragments.logs', ['title' => 'Sessions', 'pagination' => true])

@endsection

@section('footer')
    <script>
        function getCampaigns(object) {
            window.location.href = '/analytics?website_id=' + object.value;
        }
    </script>
@endsection
