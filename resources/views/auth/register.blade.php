<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Leetyics Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

    <link href="/css/sign-in.css" rel="stylesheet">
</head>
<body class="d-flex align-items-center py-4 bg-body-tertiary">
    <main class="form-signin w-100 m-auto bg-white shadow p-4">
        <form name="register" method="POST" action="{{ route('register-store') }}">
            @csrf

            <div class="text-center">
                <h1 class="h3 mb-3 fw-normal"><i class="bi bi-shield-check"></i> Leetyics</h1>
                <p>Create account at <strong>$0</strong> cost.</p>
            </div>

            @include('fragments.alerts')

            <div class="form-floating">
                <input type="email" name="email" class="form-control" id="email" placeholder="name@example.com" required value="{{ old('email') }}">
                <label for="email">Email</label>
            </div>
            <div class="form-floating">
                <input type="text" name="name" class="form-control" id="name" placeholder="Name" required value="{{ old('name') }}">
                <label for="name">Name</label>
            </div>
            <div class="form-floating">
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                <label for="password">Password</label>
            </div>
            <div class="form-floating">
                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password" required>
                <label for="password_confirmation">Confirm Password</label>
            </div>

            <button class="btn btn-primary w-100 py-2 mt-3" type="submit"><i class="bi bi-box-arrow-in-right"></i> Register</button>

            <div class="mt-3 text-center">Already have account? Please, <a href="{{ route('login') }}">login</a>.</div>
        </form>
    </main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>
