<?php

use App\Http\Controllers\AnalyticsController;
use App\Http\Controllers\CampaignsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\WebsitesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'login'])->name('login-attempt');
Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store'])->name('register-store');

Route::middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

    Route::name('websites.')->prefix('websites')->group(function () {
        Route::get('/', [WebsitesController::class, 'index'])->name('index');
        Route::get('/create', [WebsitesController::class, 'create'])->name('create');
        Route::post('/', [WebsitesController::class, 'store'])->name('store');
        Route::get('/{website}/analytics', [WebsitesController::class, 'analytics'])->name('analytics');
        Route::get('/{website}/status/{status}', [WebsitesController::class, 'status'])->name('status');
    });

    Route::name('campaigns.')->prefix('campaigns')->group(function () {
        Route::get('/', [CampaignsController::class, 'index'])->name('index');
        Route::get('/create', [CampaignsController::class, 'create'])->name('create');
        Route::post('/', [CampaignsController::class, 'store'])->name('store');
        Route::get('/{campaign}/status/{status}', [CampaignsController::class, 'status'])->name('status');
    });

    Route::name('analytics.')->prefix('analytics')->group(function () {
        Route::get('/', [AnalyticsController::class, 'index'])->name('index');
        Route::get('/{uuid}', [AnalyticsController::class, 'show'])->name('show');
    });
});
