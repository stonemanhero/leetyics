<?php

namespace App\Enums;

enum TimeFrame: string
{
    case today = 'Today';
    case this_month = 'This Month';
    case last_month = 'Last Month';
    case this_year = 'This Year';

}
