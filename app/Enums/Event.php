<?php

namespace App\Enums;


enum Event: string
{
    case click = 'Click';
    case beforeunload = 'Unload Page';
    case load = 'Load Page';
}
