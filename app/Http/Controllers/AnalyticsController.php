<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Website;
use App\Services\AnalyticsService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnalyticsController extends Controller
{
    public function __construct(protected AnalyticsService $analyticsService)
    {}

    /**
     * @param Request $request
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(Request $request): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $selectedWebsite = 'all';
        $selectedCampaign = 'all';
        $selectedTimeframe = 'today';

        if($request->has('website_id')) {
            $selectedWebsite = $request->website_id;
        }

        if($request->has('campaign_id')) {
            $selectedCampaign = $request->campaign_id;
        }

        if($request->has('timeframe')) {
            $selectedTimeframe = $request->timeframe;
        }

        $logs = $this->analyticsService->reportSessions($selectedWebsite, $selectedCampaign, $selectedTimeframe);
        $websites = Website::where('user_id', Auth::id())->get();

        $campaigns = [];

        if($selectedWebsite != 'all') {
            $campaigns = Campaign::where('website_id', $selectedWebsite)->get();
        }

        $sectionTitle = 'Analytics';

        return view('analytics.index')
            ->with('campaigns', $campaigns)
            ->with('websites', $websites)
            ->with('logs', $logs)
            ->with('selectedWebsite', $selectedWebsite)
            ->with('selectedCampaign', $selectedCampaign)
            ->with('selectedTimeframe', $selectedTimeframe)
            ->with('sectionTitle', $sectionTitle);
    }

    /**
     * @param Request $request
     * @param $uuid
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function show(Request $request, $uuid): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $selectedTimeframe = 'today';
        $selectedEvent = 'all';
        $selectedOrderBy = 'desc';

        if($request->has('timeframe')) {
            $selectedTimeframe = $request->timeframe;
        }

        if($request->has('event')) {
            $selectedEvent = $request->event;
        }

        if($request->has('order_by')) {
            $selectedOrderBy = $request->order_by;
        }

        $logs = $this->analyticsService->reportSession($uuid, $selectedEvent, $selectedTimeframe, $selectedOrderBy);

        $sectionTitle = 'Analytics - Session: ' . $uuid;

        return view('analytics.show')
            ->with('uuid', $uuid)
            ->with('logs', $logs)
            ->with('selectedTimeframe', $selectedTimeframe)
            ->with('selectedEvent', $selectedEvent)
            ->with('selectedOrderBy', $selectedOrderBy)
            ->with('sectionTitle', $sectionTitle);
    }
}
