<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignStoreRequest;
use App\Models\Campaign;
use App\Models\Website;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CampaignsController extends Controller
{
    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $sectionTitle = 'Campaigns';

        $campaigns = Campaign::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view('campaigns.index')
            ->with('campaigns', $campaigns)
            ->with('sectionTitle', $sectionTitle);
    }

    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function create(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $sectionTitle = 'Campaigns - Create';

        $websites = Website::where('user_id', Auth::id())
            ->orderBy('name', 'asc')
            ->get();

        return view('campaigns.create')
            ->with('websites', $websites)
            ->with('sectionTitle', $sectionTitle);
    }

    /**
     * @param CampaignStoreRequest $request
     * @return RedirectResponse
     */
    public function store(CampaignStoreRequest $request): RedirectResponse
    {
        $campaign = new Campaign();
        $campaign->user_id = Auth::id();
        $campaign->name = $request->name;
        $campaign->website_id = $request->website_id;
        $campaign->uuid = Str::uuid();
        $campaign->description = $request->description;
        $campaign->save();

        return redirect()->route('campaigns.index')
            ->with('success', 'You have successfully created new campaign.');
    }

    /**
     * @param Campaign $campaign
     * @param $status
     * @return RedirectResponse
     */
    public function status(Campaign $campaign, $status): RedirectResponse
    {
        if($campaign->user_id != Auth::id()) {
            abort(403);
        }

        if(!in_array($status, ['start', 'stop'])) {
            return redirect()->back()->with('error', 'Wrong status!');
        }

        $campaign->is_active = $status == 'start';
        $campaign->save();

        return redirect()->route('campaigns.index')->with('success', "Status has been successfully updated.");
    }
}
