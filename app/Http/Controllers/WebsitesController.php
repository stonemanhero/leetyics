<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebsiteStoreRequest;
use App\Models\Website;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class WebsitesController extends Controller
{
    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $sectionTitle = 'Websites';

        $websites = Website::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view('websites.index')
            ->with('websites', $websites)
            ->with('sectionTitle', $sectionTitle);
    }

    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function create(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $sectionTitle = 'Websites - Create';

        return view('websites.create')
            ->with('sectionTitle', $sectionTitle);
    }

    /**
     * @param WebsiteStoreRequest $request
     * @return RedirectResponse
     */
    public function store(WebsiteStoreRequest $request): RedirectResponse
    {
        $website = new Website();
        $website->user_id = Auth::id();
        $website->uuid = Str::uuid();
        $website->name = $request->name;
        $website->url = $request->url;
        $website->save();

        return redirect()->route('websites.index')
            ->with('success', 'You have successfully added new website.');
    }

    /**
     * @param Website $website
     * @param $status
     * @return RedirectResponse
     */
    public function status(Website $website, $status): RedirectResponse
    {
        if($website->user_id != Auth::id()) {
            abort(403);
        }

        if(!in_array($status, ['start', 'stop'])) {
            return redirect()->back()->with('error', 'Wrong status!');
        }

        $website->is_active = $status == 'start';
        $website->save();

        return redirect()->route('websites.index')->with('success', "Status has been successfully updated.");
    }
}
