<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\TrackedSession;
use App\Models\Website;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class TrackerController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function track(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->json()->all();

        $responseData = [
            'success' => true
        ];

        // Create new ClientUUID to store in Cookies if it's missing
        $clientUUID = !empty($data['clientUUID']) ? $data['clientUUID'] : null;

        if(empty($clientUUID)) {
            $clientUUID = Str::uuid();
            $responseData['clientUUID'] = $clientUUID;
        }

        // Validate Website
        $website = Website::where('uuid', $request['websiteUUID'])
            ->first();

        if(empty($website)) {
            return response()->json([
                'success' => false,
                'message' => "Website doesn't exist."
            ]);
        }

        if(!$website->is_active) {
            return response()->json([
                'success' => false,
                'message' => "Tracking has been stopped."
            ]);
        }

        // Validate Campaign
        $campaignUUID = !empty($data['campaignUUID']) ? $data['campaignUUID'] : null;

        if(!empty($campaignUUID)) {
            $campaign = Campaign::where('website_id', $website->id)
                ->where('uuid', $campaignUUID)
                ->where('is_active', true)
                ->first();
        }

        $path = parse_url($request->url);

        $track = new TrackedSession();
        $track->uuid = $clientUUID;
        $track->website_id = $website->id;

        if(!empty($campaign)) {
            $track->campaign_id = $campaign->id;
        }

        $track->event = $request['type'];
        $track->tag = $request['tag'];
        $track->ip = $request->ip();
        $track->user_agent = $request->userAgent();
        $track->path = $path['path'];
        $track->href = $request['href'];
        $track->recorded_at = Carbon::createFromFormat('Y-m-d\TH:i:s.u\Z', $request->timestamp)->format('Y-m-d H:i:s');
        $track->save();

        return response()->json($responseData);
    }
}
