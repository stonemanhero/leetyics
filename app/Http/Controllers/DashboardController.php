<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\TrackedSession;
use App\Models\Website;
use App\Services\AnalyticsService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct(protected AnalyticsService $analyticsService)
    {}


    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $logs = $this->analyticsService->reportSessions();

        $totalWebsites = Website::where('user_id', Auth::id())->count();
        $totalCampaigns = Campaign::where('user_id', Auth::id())->count();
        $totalSessions = TrackedSession::select('tracked_sessions.uuid')
            ->leftJoin('websites', 'websites.id', '=', 'tracked_sessions.website_id')
            ->where('websites.user_id', Auth::id())
            ->count();

        $sectionTitle = 'Dashboard';

        return view('dashboard.index')
            ->with('totalWebsites', $totalWebsites)
            ->with('totalCampaigns', $totalCampaigns)
            ->with('totalSessions', $totalSessions)
            ->with('logs', $logs)
            ->with('sectionTitle', $sectionTitle);
    }
}
