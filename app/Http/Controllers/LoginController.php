<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * @return View|Application|Factory|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        if(Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('auth.login');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $user = User::where('email', $request->get('email'))->first();

        if(empty($user)) {
            return back()->withInput()->with('error', 'Wrong username or password!');
        }

        if($user->isLocked()) {
            return back()->withInput()->with('error', 'This account has been locked out.');
        }

        if(Auth::attempt($request->except('_token'), $request->has('rememberme')))
        {
            $request->session()->regenerate();

            $user->clearFailedAttempts();

            return redirect()->route('dashboard')
                ->with('success', 'You have successfully logged in!');
        } else {
            $user->increaseFailedAttempts();
        }

        return back()->withInput()->with('error', 'Wrong username or password!');

    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('login')->with('success', 'You have successfully logged out.');
    }
}
