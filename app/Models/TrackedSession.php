<?php

namespace App\Models;

use App\Enums\Event;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackedSession extends Model
{
    use HasFactory;

    protected $casts = [
        'recorded_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * @return string|void
     */
    public function eventName()
    {
        foreach(Event::cases() as $case) {
            if($case->name == $this->event) {
                return $case->value;
            }
        }
    }
}
