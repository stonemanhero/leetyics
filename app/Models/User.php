<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'failed_login_attempts'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
        'failed_login_attempts' => 'integer'
    ];

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->failed_login_attempts >= 3;
    }

    /**
     * @return void
     */
    public function increaseFailedAttempts(): void
    {
        $this->failed_login_attempts += 1;
        $this->save();
    }

    /**
     * @return void
     */
    public function clearFailedAttempts(): void
    {
        $this->failed_login_attempts = 0;
        $this->save();
    }
}
