<?php

namespace App\Services;


use App\Models\TrackedSession;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnalyticsService
{
    /**
     * @param string $website
     * @param string $campaign
     * @param string $timeframe
     * @return mixed
     */
    public function reportSessions(string $website = 'all', string $campaign = 'all', string $timeframe = 'today'): mixed
    {
        $dates = $this->calculateDates($timeframe);

        $logsQuery = TrackedSession::select(DB::raw("websites.id as website_id, websites.user_id as user_id, websites.name as website_name,
            websites.url as website_url, campaigns.id as campaign_id, campaigns.name as campaign_name, tracked_sessions.recorded_at,
            tracked_sessions.uuid, tracked_sessions.ip"))
            ->leftJoin('websites', 'websites.id', '=', 'tracked_sessions.website_id')
            ->leftJoin('campaigns', 'campaigns.id', '=', 'tracked_sessions.campaign_id')
            ->where('websites.user_id', Auth::id())
            ->whereDate('tracked_sessions.recorded_at', '>=', $dates['start'])
            ->whereDate('tracked_sessions.recorded_at', '<=', $dates['end']);

        if ($website !== 'all') {
            $logsQuery->where('tracked_sessions.website_id', $website);
        }

        if ($campaign !== 'all') {
            $logsQuery->where('campaign_id', $campaign);
        }

        return $logsQuery
            ->groupBy('tracked_sessions.uuid')
            ->orderBy('tracked_sessions.recorded_at', 'desc')
            ->paginate(15)
            ->withQueryString();
    }

    /**
     * @param $uuid
     * @param string $event
     * @param string $timeframe
     * @param string $orderBy
     * @return mixed
     */
    public function reportSession($uuid, string $event='all', string $timeframe='today', string $orderBy='desc'): mixed
    {
        $dates = $this->calculateDates($timeframe);

        $sessionQuery = TrackedSession::select('tracked_sessions.*', 'websites.user_id')
            ->leftJoin('websites', 'websites.id', '=', 'tracked_sessions.website_id')
            ->where('tracked_sessions.uuid', $uuid)
            ->where('websites.user_id', Auth::id())
            ->whereDate('tracked_sessions.recorded_at', '>=', $dates['start'])
            ->whereDate('tracked_sessions.recorded_at', '<=', $dates['end']);

        if($event !== 'all') {
            $sessionQuery->where('event', $event);
        }

        return $sessionQuery->orderBy('tracked_sessions.id', $orderBy)
            ->paginate(15)
            ->withQueryString();
    }

    /**
     * @param $timeframe
     * @return array
     */
    private function calculateDates($timeframe): array
    {
        $startDate = Carbon::now()->startOfDay();
        $endDate = Carbon::now();

        if($timeframe == 'this_month') {
            $startDate = Carbon::now()->startOfMonth();
            $endDate = Carbon::now()->endOfMonth();
        } else if($timeframe == 'last_month') {
            $startDate = Carbon::now()->subMonth()->startOfMonth();
            $endDate = Carbon::now()->subMonth()->endOfMonth();
        } else if($timeframe == 'this_year'){
            $startDate = Carbon::now()->startOfYear();
        }

        return [
            'start' => $startDate,
            'end' => $endDate
        ];
    }
}
