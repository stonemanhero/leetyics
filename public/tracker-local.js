class Tracker {
    clientUUID;
    campaignUUID;
    availableNodeNames = [
        'BUTTON',
        'A',
        'UL',
        'LI',
        'INPUT',
        'TEXTAREA',
        '#document'
    ];

    constructor(apiUrl) {
        this.apiUrl = apiUrl;
        this.websiteUUID = this.getWebsiteUUID();
        this.campaignUUID = this.getCampaignUUID();
        this.clientUUID = this.getClientUUID();

        onload = (event) => this.resolve(event);
        onclick = (event) => this.resolve(event);
        onbeforeunload = (event) => this.resolve(event);
    }

    resolve = event => {
        if(this.availableNodeNames.includes(event.target.nodeName)) {
            const data = {
                clientUUID: this.clientUUID,
                websiteUUID: this.websiteUUID,
                campaignUUID: this.campaignUUID,
                type: event.type,
                tag: event.target.tagName !== undefined ? event.target.tagName : '',
                label: event.target.innerText !== undefined ? event.target.innerText : '',
                href: event.target.href !== undefined ? event.target.href : '',
                url: window.location.href,
                timestamp: new Date().toISOString()
            }

            this.track(data).then((response) => {
                this.setClientUUID(response);
            });
        }
    };

    track = async data => {
        try {
            const response = await fetch(this.apiUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });

            if(!response.ok) {
                console.log('Error! Failed to post data to server.');
            }

            return response.json();
        } catch(error) {
            console.error('Error! Failed to post data to server:', error);
        }
    };

    getWebsiteUUID = () => {
        const scriptUrl = document.querySelector('script[src^="http://leetyics.local/"]').getAttribute('src');
        return (new URL(scriptUrl)).searchParams.get('uuid');
    };


    // Cookies - Client UUID
    getClientUUID = () => {
        return document.cookie.split("; ")
            .find((row) => row.startsWith("client_uuid="))
            ?.split("=")[1];

    }

    setClientUUID = response => {
        if(response.clientUUID !== undefined && this.clientUUID === undefined) {
            this.clientUUID = response.clientUUID;
            document.cookie = "client_uuid=" + this.clientUUID;
        }
    };

    // Cookies - Campaign UUID
    getCampaignUUID = () => {
        let campaign_uuid = document.cookie.split("; ")
            .find((row) => row.startsWith("campaign_uuid="))
            ?.split("=")[1];

        if(campaign_uuid !== undefined) {
            return campaign_uuid;
        }

        campaign_uuid = this.findCampaignUUID();

        if(campaign_uuid !== undefined) {
            document.cookie = "campaign_uuid=" + campaign_uuid;
        }

        return campaign_uuid;
    }

    findCampaignUUID = () => {
        return (new URL(window.location.href)).searchParams.get('campaign');
    }
}

new Tracker('http://leetyics.local/api/v1/track');
